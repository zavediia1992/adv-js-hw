

class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;

    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}




class Programmer extends Employee {
    get salary () {
        return this._salary*3;
    }
}

const vasa = new Employee("vasa", 20, 300);
console.log(vasa.salary);
const programmer1 = new Programmer("vasa", 30, 1000);
const programmer2 = new Programmer("john", 31, 3000);

console.log(programmer1);
console.log(programmer2);

console.log(programmer2.salary);

